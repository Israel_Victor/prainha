<?php
$title = ""; // palavra chave 
$title2 = ""; // tag title da página
$keywords = ""; // meta keywords
$description = ""; // meta description
$link = $_SERVER["REQUEST_URI"]; // url canonica para as páginas do site

include("includes/head.php");
include("includes/header.php");

$nomeUrl = glob("*.php");
$pagina = '';

foreach ($nomeUrl as $v) {

    $nomeLink = str_replace(['.php'], [''], $v); // renomeia o valor dentro do atributo href="ex:valor"
    $nomeLista = str_replace(['.php', '-'], ['', ' '], $v); // renomeia o valor dentro da tag <a>ex:valor</a> 

    if ($v != "index.php" && $v != "quem-somos.php" && $v != "contato.php" && $v != "phpinfo.php" && $v != "404.php" && $v != "mapa-site.php") {
        $pagina .= '<li><a href="' . $nomeLink . '">' . $nomeLista . '</a></li>';
    }
}
?>

<main>
    <section>
        <div class="container">
            <div class="row">
                <ul>
                    <?= $pagina ?> <!-- todas as urls da pasta raiz -->
                </ul>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>
</main>
<!-- /.main -->

<?php include("includes/footer.php") ?>
<!-- /.footer -->