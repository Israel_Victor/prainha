const path = require('path');
const webpack = require('webpack');
const MiniCssExtracPlugin = require('mini-css-extract-plugin');

module.exports = {
    mode: 'development', // modo webpack 
    entry: './src/js/index.js',
    output: {
        filename: "[name].js", // arquivo 
        path: path.resolve(__dirname, 'dist'),
        publicPath: ''
    },
    module: {
        rules: [
            {
                test: /\.(sa|sc|c)ss$/,
                use: [
                    MiniCssExtracPlugin.loader,
                    'css-loader',
                    'sass-loader'
                ]
            },
            {
                test: /\.css$/i,
                use: [
                    'style-loader',
                    'css-loader'
                ]
            },
            {
                test: /\.(jpg|jpeg|gif|png)$/,
                use: {
                    loader: 'file-loader',
                    options: {
                        name: '[name].[ext]',
                        publicPath: 'images',
                        outputPath: 'images',
                    }
                }
            },
            {
                test: /\.(eot|ttf|woff|woff2)$/,
                use: {
                    loader: 'file-loader',
                    options: {
                        name: '[name].[ext]',
                        publicPath: 'fonts',
                        outputPath: 'fonts'
                    }
                }
            },
            {
                test: /\.svg$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            limit: 800
                        }
                    }
                ]
            }
        ]
    },
    plugins: [
        new MiniCssExtracPlugin({
            filename: "styles.css"
        }),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            "window.jQuery": "jquery"
        })
    ],
    resolve: {
        alias: {
            "matches-selector/matches-selector": "desandro-matches-selector",
            "eventEmitter/EventEmitter": "wolfy87-eventemitter",
            "get-style-property/get-style-property": "desandro-get-style-property",
            "masonry/masonry": "outlayer"
        }
    }
}