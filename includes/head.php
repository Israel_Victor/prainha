<?php
$ano = date('Y');
$nomeEmpresa = "";
$language = "portuguese, português";
$author = "https://publinet1.com.br";
$nomeAutor = "Publinet Ltda";
$segmento = "";
$copyright = "&copy $nomeEmpresa $ano - Todos os direitos reservados";
$url = "https://";
$endereco = "";
$bairroHead = "";
$cidade = "";
$estado = "SP";
$pais = "Brasil";
$latitude = "";
$longitude = "";
$googleverification = "";
$email = "";
$ddd = "(11)";
$whatsapp = "$ddd ";
$telefone1 = "$ddd ";
$telefone2 = "";
$urlImagem = "images/capa.jpg";
$urlGooglePlus = "";
$twitter = "";
$facebookID = "";
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>

    <title><?= $title2 ?></title>
    <meta name="title" content="<?= $title ?>" />
    <meta name="keywords" content="<?= $keywords ?>" />
    <meta name="description" content="<?= $description ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="language" content="pt-br" />
    <meta name="author" content="<?= $author ?>" />
    <meta name="copyright" content="<?= $nomeEmpresa ?>" />
    <meta name="distribution" content="global" />
    <meta name="audience" content="all" />
    <meta name="url" content="<?= $url ?>" />
    <meta name="classification" content="<?= $segmento ?>" />
    <meta name="category" content="<?= $segmento ?>" />
    <meta name="Page-Topic" content="<?= $title ?>" />
    <meta name="rating" content="general" />
    <meta name="fone" content="<?= $telefone1 ?>" />
    <meta name="city" content="<?= $cidade ?> - <?= $estado ?>" />
    <meta name="State" content="<?= $cidade ?> - <?= $estado ?>" />
    <meta name="country" content="<?= $pais ?>" />
    <meta property="publisher" content="<?= $nomeAutor ?>" />
    <link rel="shortcut icon" href="images/favicon.ico">

    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="google-site-verification" content="<?= $googleverification ?>" />
    <meta name="robots" content="index, follow" />
    <meta http-equiv="Content-Language" content="pt" />
    <meta name="revisit-after" content="1 days" />
    <meta http-equiv="expires" content="never" />
    <meta name="DC.title" content="<?= $title ?>" />
    <meta name="ICBM" content="<?= $title ?>" />
    <meta name="geo.placename" content="<?= "$cidade" ?>" />
    <meta name="geo.position" content="<?= $latitude ?>,<?= $longitude ?>" />
    <meta name="geo.region" content="br" />
    <meta name="ICBM" content="<?= $latitude ?>,<?= $longitude ?>" />

    <link rel="canonical" href="<?= $url . $link ?>" />
    <meta name="robots" content="index, follow" />
    <meta name="googlebot" content="index, follow" />
    <meta name="revisit-after" content="2 days" />
    <meta name="geo.placename" content="<?= $pais ?>" />
    <meta name="geo.region" content="<?= "$cidade - $estado" ?>" />
    <meta itemprop="name" content="<?= $pais ?>" />
    <meta itemprop="description" content="<?= $description ?>" />
    <meta itemprop="image" content="<?= "$url/$urlImagem" ?>" />
    <link rel="author" href="<?= $urlGooglePlus ?>" />
    <link rel="publisher" href="<?= $urlGooglePlus ?>" />

    <meta name="twitter:card" content="<?= "$url/$urlImagem" ?>" />
    <meta name="twitter:site" content="<?= $url . $link ?>" />
    <meta name="twitter:title" content="<?= $title ?>" />
    <meta name="twitter:description" content="<?= $description ?>">
    <meta name="twitter:creator" content="<?= $twitter ?>" />
    <meta name="twitter:image:src" content="<?= "$url/$urlImagem" ?>" />

    <meta property="og:title" content="<?= $title ?>" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="<?= $url . $link ?>" />
    <meta property="og:site_name" content="<?= $nomeEmpresa ?>" />
    <meta property="og:locale" content="pt_BR" />
    <meta property="og:image" content="<?= "$url/$urlImagem" ?>" />
    <meta property="og:image:type" content="image/jpg" />
    <meta property="og:image:width" content="500" />
    <meta property="og:image:height" content="250" />
    <meta property="og:description" content="<?= $description ?>" />
    <meta property="fb:admins" content="<?= $facebookID ?>" />
    <link href="<?= "$url/$urlImagem" ?>" rel="image_src">

    <!-- CSS e/ou JS  -->

    <!-- BUNDLE CSS  -->
    <link rel="stylesheet" href="dist/styles.css">
</head>