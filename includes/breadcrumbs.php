<ul itemscope itemtype="http://schema.org/BreadcrumbList" class="breadcrumbs-custom">
    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="index" title="<?= $title2 ?>">
            <span itemprop="name">Home</span></a>
        <meta itemprop="position" content="1" />
    </li>
    <i class="fa fa-angle-right"></i>
    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="<?= $link ?>" title="<?= $title2 ?>">
            <span itemprop="name"><?= $title2 ?></span></a>
        <meta itemprop="position" content="2" />
    </li>
</ul>