<body>
    <header>
        <div class="pre-nav">
            <div class="row d-flex align-items-center">
                <div class="col-md-6">
                    <div class="float-left pre-nav-text">
                        <i class="fas fa-map-marker-alt"></i>
                        Endereço: <strong>Rua Diamantina, 331 - Vl. Maria</strong>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="float-right pre-nav-text">
                        <i class="fab fa-whatsapp"></i>
                        <strong>(11) 94805-3542</strong>
                    </div>
                </div>
                <div class="col-md-1 pre-nav-icons">
                    <ul class="d-flex justify-content-around align-items-center">
                        <li>
                        <a href="https://www.facebook.com/496857837063646/" target="_blank"><i class="fab fa-facebook"></i></a>
                        </li>
                        <li>
                        <a href="https://instagram.com/prainha_da_vila_bar?igshid=1n47w42kxswzb" target="_blank"><i class="fab fa-instagram"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#">
                <img src="src/imgs/logo.png" alt="<?php $title ?>">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav container m-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Features</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Pricing</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Dropdown link
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
    </header>